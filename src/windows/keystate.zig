const builtin = @import("builtin");
const std = @import("std");

const win_types = @import("win_types.zig");
const win32 = @import("win32.zig");

const KeySym = win_types.KeySym;

pub fn keybind_array_to_keysym_array(
    allocator: std.mem.Allocator,
    keybinds: []const []const u8,
) ![]const KeySym {
    var out = std.ArrayList(KeySym).init(allocator);
    defer out.deinit();

    for (keybinds) |keybind| {
        if (std.meta.stringToEnum(win32.VIRTUAL_KEY, keybind)) |keysym| {
            try out.append(@intFromEnum(keysym));
            continue;
        }

        return error.FailedToParseKeybind;
    }

    return out.toOwnedSlice();
}

pub fn are_all_keysym_pressed(keys: []const KeySym) bool {
    if (keys.len == 0) return false;

    var pressed_count: usize = 0;
    for (keys) |key| {
        const result: i32 = @intCast(win32.GetKeyState(key));
        pressed_count += @intFromBool((result & 0x8000) != 0);
    }

    return pressed_count == keys.len;
}
