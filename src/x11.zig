const builtin = @import("builtin");
const std = @import("std");

const keystate = @import("x11/keystate.zig");
const x11_types = @import("x11/x11_types.zig");
const x = @import("x11/x11_wrapper.zig");

pub const unmanaged = x;

pub const Atom = x11_types.Atom;
pub const Display = x11_types.Display;
pub const KeyCode = x11_types.KeyCode;
pub const KeySym = x11_types.KeySym;
pub const pid_t = @import("pid_type.zig").pid_t;
pub const Window = x11_types.Window;

pub const keybind_array_to_keysym_array = keystate.keybind_array_to_keysym_array;
pub const are_all_keysym_pressed_unmanaged = keystate.are_all_keysym_pressed_unmanaged;

var display_handle: ?*Display = null;

/// Allocations are short lived, and are free'd before this function returns.
pub fn init(allocator: std.mem.Allocator) !void {
    display_handle = try x.open_display(allocator);
}

pub fn deinit() void {
    x.close_display(display_handle.?);
}

pub fn are_all_keysym_pressed(keys: []const KeySym) bool {
    return are_all_keysym_pressed_unmanaged(display_handle.?, keys);
}

pub fn set_error_handler(handler: ?*const x11_types.XErrorHandler) ?*const x11_types.XErrorHandler {
    return x.set_error_handler(handler);
}

pub fn get_focused_window() Window {
    return x.get_input_focus(display_handle.?);
}

pub fn get_root_window() Window {
    return x.default_root_window(display_handle.?);
}

fn _try_get_window_pid(display: *Display, window: Window) ?pid_t {
    const _NET_WM_PID = x.intern_atom(display, "_NET_WM_PID", true);

    if (x.get_window_property(display, window, _NET_WM_PID, .xa_cardinal, false)) |prop| {
        defer prop.deinit();

        if (prop.actual_type == .xa_cardinal and prop.actual_format == 32 and prop.number_of_items == 1) {
            if (prop.data) |data| {
                const pid = std.mem.readInt(isize, data[0..@sizeOf(isize)], builtin.cpu.arch.endian());
                return std.math.cast(pid_t, pid);
            }
        }
    }

    return null;
}

pub fn try_get_window_pid(window: Window) ?pid_t {
    return _try_get_window_pid(display_handle.?, window);
}

fn search_window_and_children_for_pid(display: *Display, window: Window, pid: pid_t) ?Window {
    if (x.query_tree(display, window)) |query| {
        defer query.deinit();

        if (query.children) |children| {
            for (children) |child_window| {
                if (_try_get_window_pid(display, child_window)) |child_pid| {
                    if (child_pid == pid) return child_window;
                } else if (search_window_and_children_for_pid(display, child_window, pid)) |deeper_result| {
                    return deeper_result;
                }
            }
        }
    }

    return null;
}

pub fn search_all_windows_for_pid(pid: pid_t) ?Window {
    const root_window = get_root_window();
    return search_window_and_children_for_pid(display_handle.?, root_window, pid);
}

pub const WindowGeometry = struct {
    width: i32,
    height: i32,
    border_width: i32,
};

pub fn try_get_window_geometry(window: Window) ?WindowGeometry {
    if (x.get_window_attributes(display_handle.?, window)) |attrs| {
        return .{
            .width = attrs.width,
            .height = attrs.height,
            .border_width = attrs.border_width,
        };
    }

    return null;
}

pub fn query_pointer_position_in_window(window: Window) x.PointerQuery {
    return x.query_pointer(display_handle.?, window);
}
