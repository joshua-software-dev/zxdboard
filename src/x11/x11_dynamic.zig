const builtin = @import("builtin");
const std = @import("std");

const options = @import("zxdboard_options");
const x11_types = @import("x11_types.zig");

const Atom = x11_types.Atom;
const Display = x11_types.Display;
const KeyCode = x11_types.KeyCode;
const KeySym = x11_types.KeySym;
const Window = x11_types.Window;

const X11 = struct {
    pub const XCloseDisplay = fn (
        display: ?*Display,
    ) callconv(.C) i32; // c_int

    pub const XDefaultRootWindow = fn (
        display: ?*Display,
    ) callconv(.C) Window;

    pub const XFree = fn (
        data: *const anyopaque,
    ) callconv(.C) i32; // c_int

    pub const XGetInputFocus = fn (
        display: ?*Display,
        keys_return: *Window,
        revert_to_return: *i32, // c_int
    ) callconv(.C) i32; // c_int

    pub const XGetWindowAttributes = fn (
        display: ?*Display,
        w: Window,
        window_attributes_return: *x11_types.XWindowAttributes,
    ) callconv(.C) i32; // c_int

    pub const XGetWindowProperty = fn (
        display: ?*Display,
        w: Window,
        property: Atom,
        long_offset: isize, // c_long
        long_length: isize, // c_long
        delete: i32, // bool
        req_type: Atom,
        actual_type_return: *Atom,
        actual_format_return: *i32, // c_int
        nitems_return: *usize, // c_ulong
        bytes_after_return: *usize, // c_ulong
        prop_return: *?[*:0]u8, // unsigned char**
    ) callconv(.C) i32; // c_int

    pub const XInternAtom = fn (
        display: ?*Display,
        atom_name: [*:0]const u8, // [*:0]const c_char
        only_if_exists: i32, // bool
    ) callconv(.C) Atom;

    pub const XKeysymToKeycode = fn (
        display: ?*Display,
        keysym: KeySym,
    ) callconv(.C) KeyCode;

    pub const XOpenDisplay = fn (
        display_name: [*:0]const u8, // [*:0]const c_char
    ) callconv(.C) ?*Display;

    pub const XQueryKeymap = fn (
        display: ?*Display,
        keys_return: *[32]u8, // char [32]
    ) callconv(.C) i32; // c_int

    pub const XQueryPointer = fn (
        display: ?*Display,
        w: Window,
        root_return: *Window,
        child_return: *Window,
        root_x_return: *i32, // c_int
        root_y_return: *i32, // c_int
        win_x_return: *i32, // c_int
        win_y_return: *i32, // c_int
        mask_return: *u32, // c_uint
    ) callconv(.C) i32; // bool

    pub const XQueryTree = fn (
        display: ?*Display,
        w: Window,
        root_return: *Window,
        parent_return: *Window,
        children_return: *?[*]Window,
        nchildren_return: *u32, // c_uint
    ) callconv(.C) i32; // c_int

    pub const XSetErrorHandler = fn (
        handler: ?*const x11_types.XErrorHandler,
    ) callconv(.C) ?*const x11_types.XErrorHandler;

    pub const XStringToKeysym = fn (
        string: [*:0]const u8, // [*:0]const c_char
    ) callconv(.C) KeySym;
};

var libX11: ?std.DynLib = null;
pub var XCloseDisplay: ?*const X11.XCloseDisplay = null;
pub var XDefaultRootWindow: ?*const X11.XDefaultRootWindow = null;
pub var XFree: ?*const X11.XFree = null;
pub var XGetInputFocus: ?*const X11.XGetInputFocus = null;
pub var XGetWindowAttributes: ?*const X11.XGetWindowAttributes = null;
pub var XGetWindowProperty: ?*const X11.XGetWindowProperty = null;
pub var XInternAtom: ?*const X11.XInternAtom = null;
pub var XKeysymToKeycode: ?*const X11.XKeysymToKeycode = null;
pub var XOpenDisplay: ?*const X11.XOpenDisplay = null;
pub var XQueryKeymap: ?*const X11.XQueryKeymap = null;
pub var XQueryPointer: ?*const X11.XQueryPointer = null;
pub var XQueryTree: ?*const X11.XQueryTree = null;
pub var XSetErrorHandler: ?*const X11.XSetErrorHandler = null;
pub var XStringToKeysym: ?*const X11.XStringToKeysym = null;

pub fn load_libx11_from_path(path: []const u8) !void {
    if (libX11 != null) return;
    libX11 = try std.DynLib.open(path);

    XCloseDisplay = libX11.?.lookup(
        *const X11.XCloseDisplay,
        "XCloseDisplay",
    ) orelse return error.SymbolLookupFailed;
    XDefaultRootWindow = libX11.?.lookup(
        *const X11.XDefaultRootWindow,
        "XDefaultRootWindow",
    ) orelse return error.SymbolLookupFailed;
    XFree = libX11.?.lookup(
        *const X11.XFree,
        "XFree",
    ) orelse return error.SymbolLookupFailed;
    XGetInputFocus = libX11.?.lookup(
        *const X11.XGetInputFocus,
        "XGetInputFocus",
    ) orelse return error.SymbolLookupFailed;
    XGetWindowAttributes = libX11.?.lookup(
        *const X11.XGetWindowAttributes,
        "XGetWindowAttributes",
    ) orelse return error.SymbolLookupFailed;
    XGetWindowProperty = libX11.?.lookup(
        *const X11.XGetWindowProperty,
        "XGetWindowProperty",
    ) orelse return error.SymbolLookupFailed;
    XInternAtom = libX11.?.lookup(
        *const X11.XInternAtom,
        "XInternAtom",
    ) orelse return error.SymbolLookupFailed;
    XKeysymToKeycode = libX11.?.lookup(
        *const X11.XKeysymToKeycode,
        "XKeysymToKeycode",
    ) orelse return error.SymbolLookupFailed;
    XOpenDisplay = libX11.?.lookup(
        *const X11.XOpenDisplay,
        "XOpenDisplay",
    ) orelse return error.SymbolLookupFailed;
    XQueryKeymap = libX11.?.lookup(
        *const X11.XQueryKeymap,
        "XQueryKeymap",
    ) orelse return error.SymbolLookupFailed;
    XQueryPointer = libX11.?.lookup(
        *const X11.XQueryPointer,
        "XQueryPointer",
    ) orelse return error.SymbolLookupFailed;
    XQueryTree = libX11.?.lookup(
        *const X11.XQueryTree,
        "XQueryTree",
    ) orelse return error.SymbolLookupFailed;
    XSetErrorHandler = libX11.?.lookup(
        *const X11.XSetErrorHandler,
        "XSetErrorHandler",
    ) orelse return error.SymbolLookupFailed;
    XStringToKeysym = libX11.?.lookup(
        *const X11.XStringToKeysym,
        "XStringToKeysym",
    ) orelse return error.SymbolLookupFailed;
}

pub fn load_libx11() !void {
    if (!builtin.link_libc and options.libx11_path == null) {
        @compileError("Error: libX11 must be init with the full path to the library object.");
    }

    try load_libx11_from_path(
        if (options.libx11_path) |libx11_path|
            libx11_path
        else
            "libX11.so.6",
    );
}

pub fn unload_libx11() void {
    if (libX11) |*lib| lib.close();
    libX11 = null;
}
