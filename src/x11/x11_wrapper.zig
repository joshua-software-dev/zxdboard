const builtin = @import("builtin");
const std = @import("std");

const x11_types = @import("x11_types.zig");
const x11 = blk: {
    const options = @import("zxdboard_options");
    if (options.use_system_x11) break :blk @import("x11_static.zig");
    break :blk @import("x11_dynamic.zig");
};

const Atom = x11_types.Atom;
const Display = x11_types.Display;
const KeyCode = x11_types.KeyCode;
const KeySym = x11_types.KeySym;
const pid_t = @import("../pid_type.zig").pid_t;
const Window = x11_types.Window;

pub fn close_display(display: *Display) void {
    if (comptime builtin.object_format != .elf) {
        @compileError("Non-Elf based Operating System unsupported");
    }

    // if XCloseDisplay is not yet loaded, silently eliding it is an acceptable
    // way to prevent difficult to catch errors.
    if (x11.XCloseDisplay) |XCloseDisplay| {
        // XCloseDisplay always returns 0
        _ = XCloseDisplay(display);
    }
    x11.unload_libx11();
}

pub fn default_root_window(display: *Display) Window {
    return x11.XDefaultRootWindow.?(display);
}

pub fn free(data: *const anyopaque) i32 {
    return x11.XFree.?(data);
}

pub fn get_input_focus(display: *Display) Window {
    var window: Window = .null_handle;
    var revert: i32 = 0;
    // XGetInputFocus always returns 1
    _ = x11.XGetInputFocus.?(display, &window, &revert);
    return window;
}

pub fn get_window_attributes(display: *Display, window: Window) ?x11_types.XWindowAttributes {
    var attrs: x11_types.XWindowAttributes = undefined;
    if (x11.XGetWindowAttributes.?(display, window, &attrs) == 0) {
        // failure
        return null;
    }

    return attrs;
}

pub const WindowProperty = struct {
    actual_type: Atom,
    actual_format: i32,
    number_of_items: usize,
    bytes_after_return_value: usize,
    data: ?[*:0]u8,

    pub fn deinit(self: @This()) void {
        if (self.data) |data| {
            _ = free(@ptrCast(data));
        }
    }
};

pub fn get_window_property(
    display: *Display,
    window: Window,
    desired_property: Atom,
    expected_property_type: Atom,
    delete_property_from_window: bool,
) ?WindowProperty {
    var atom_type_return_value: Atom = .null_handle;
    var format_return_value: i32 = 0;
    var number_of_items_return_value: usize = 0;
    var bytes_after_return_value: usize = 0;
    var property_return_value: ?[*:0]u8 = null;

    if (x11.XGetWindowProperty.?(
        display,
        window,
        desired_property,
        0,
        1024,
        @as(i32, @intFromBool(delete_property_from_window)),
        expected_property_type,
        &atom_type_return_value,
        &format_return_value,
        &number_of_items_return_value,
        &bytes_after_return_value,
        &property_return_value,
    ) != 0) {
        // failure
        return null;
    }

    return .{
        .actual_type = atom_type_return_value,
        .actual_format = format_return_value,
        .number_of_items = number_of_items_return_value,
        .bytes_after_return_value = bytes_after_return_value,
        .data = property_return_value,
    };
}

/// X11 maintains a list of so called `Atom`s, which are strings that it
/// assigns a unique integer to. This function will attempt to first acquire
/// an existing `Atom` for this string, and if it does not exist, allocate a
/// new one. You can prevent registering a new atom by setting `only_if_exist`
/// to `true`.
pub fn intern_atom(display: *Display, atom_string: [:0]const u8, only_if_exist: bool) Atom {
    return x11.XInternAtom.?(display, atom_string, @intFromBool(only_if_exist));
}

pub fn keysym_to_keycode(display: *Display, keysym: KeySym) KeyCode {
    std.debug.assert(x11.XKeysymToKeycode != null);
    return x11.XKeysymToKeycode.?(display, keysym);
}

pub fn open_display_by_name(display_name: [:0]const u8) !*Display {
    if (comptime builtin.object_format != .elf) {
        @compileError("Non-Elf based Operating System unsupported");
    }

    try x11.load_libx11();
    if (x11.XOpenDisplay.?(display_name[0..].ptr)) |handle| {
        return handle;
    }

    return error.FailedToOpenDisplay;
}

/// Allocations are short lived, and are free'd before this function returns.
pub fn open_display(allocator: std.mem.Allocator) !*Display {
    if (comptime builtin.object_format != .elf) {
        @compileError("Non-Elf based Operating System unsupported");
    }

    var envmap = std.process.getEnvMap(allocator) catch return error.OutOfMemory;
    defer envmap.deinit();
    const val = envmap.get("DISPLAY") orelse return error.FailedToLookupDisplayName;
    const display_name: [:0]const u8 = try allocator.dupeZ(u8, val);
    defer allocator.free(display_name);

    return open_display_by_name(display_name);
}

pub fn query_keymap(display: *Display) [32]u8 {
    var buf: [32]u8 = undefined;

    // XQueryKeymap always returns 1
    _ = x11.XQueryKeymap.?(display, &buf);
    return buf;
}

pub const PointerQuery = struct {
    pointer_on_same_screen_as_window: bool,
    root_window: Window,
    child_window: Window,
    root_x_pos: i32,
    root_y_pos: i32,
    win_x_pos: i32,
    win_y_pos: i32,
    mask_return_value: u32,
};

pub fn query_pointer(display: *Display, window: Window) PointerQuery {
    var root_return_value: Window = .null_handle;
    var child_return_value: Window = .null_handle;
    var root_x_return_value: i32 = 0;
    var root_y_return_value: i32 = 0;
    var win_x_return_value: i32 = 0;
    var win_y_return_value: i32 = 0;
    var mask_return_value: u32 = 0;

    const same_screen = x11.XQueryPointer.?(
        display,
        window,
        &root_return_value,
        &child_return_value,
        &root_x_return_value,
        &root_y_return_value,
        &win_x_return_value,
        &win_y_return_value,
        &mask_return_value,
    );

    return .{
        .pointer_on_same_screen_as_window = same_screen > 0,
        .root_window = root_return_value,
        .child_window = child_return_value,
        .root_x_pos = root_x_return_value,
        .root_y_pos = root_y_return_value,
        .win_x_pos = win_x_return_value,
        .win_y_pos = win_y_return_value,
        .mask_return_value = mask_return_value,
    };
}

pub const TreeQuery = struct {
    root: Window,
    parent: Window,
    children: ?[]Window,

    pub fn deinit(self: @This()) void {
        if (self.children) |children| {
            if (children.len > 0) {
                _ = free(@ptrCast(children.ptr));
            }
        }
    }
};

pub fn query_tree(display: *Display, window: Window) ?TreeQuery {
    var root_return_value: Window = .null_handle;
    var parent_return_value: Window = .null_handle;
    var children_return_value: ?[*]Window = null;
    var num_children_return_value: u32 = 0;

    if (x11.XQueryTree.?(
        display,
        window,
        &root_return_value,
        &parent_return_value,
        &children_return_value,
        &num_children_return_value,
    ) == 0) {
        // failure
        return null;
    }

    return .{
        .root = root_return_value,
        .parent = parent_return_value,
        .children = blk: {
            if (children_return_value) |children|
                break :blk children[0..num_children_return_value]
            else
                break :blk null;
        },
    };
}

pub fn set_error_handler(handler: ?*const x11_types.XErrorHandler) ?*const x11_types.XErrorHandler {
    return x11.XSetErrorHandler.?(handler);
}

pub fn string_to_keysym(string: [:0]const u8) KeySym {
    return x11.XStringToKeysym.?(string[0..].ptr);
}
