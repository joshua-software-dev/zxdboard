const x11_types = @import("x11_types.zig");

const Atom = x11_types.Atom;
const Display = x11_types.Display;
const KeyCode = x11_types.KeyCode;
const KeySym = x11_types.KeySym;
const Window = x11_types.Window;

const X11 = struct {
    pub extern fn XCloseDisplay(
        display: ?*Display,
    ) i32; // c_int

    pub extern fn XDefaultRootWindow(
        display: ?*Display,
    ) Window;

    pub extern fn XFree(
        data: *const anyopaque,
    ) i32; // c_int

    pub extern fn XGetInputFocus(
        display: ?*Display,
        keys_return: *Window,
        revert_to_return: *i32, // c_int
    ) i32; // c_int

    pub extern fn XGetWindowAttributes(
        display: ?*Display,
        w: Window,
        window_attributes_return: *x11_types.XWindowAttributes,
    ) i32; // c_int

    pub extern fn XGetWindowProperty(
        display: ?*Display,
        w: Window,
        property: Atom,
        long_offset: isize, // c_long
        long_length: isize, // c_long
        delete: i32, // bool
        req_type: Atom,
        actual_type_return: *Atom,
        actual_format_return: *i32, // c_int
        nitems_return: *usize, // c_ulong
        bytes_after_return: *usize, // c_ulong
        prop_return: *?[*:0]u8, // unsigned char**
    ) i32; // c_int

    pub extern fn XInternAtom(
        display: ?*Display,
        atom_name: [*:0]const u8, // [*:0]const c_char
        only_if_exists: i32, // bool
    ) Atom;

    pub extern fn XKeysymToKeycode(
        display: ?*Display,
        keysym: KeySym,
    ) KeyCode;

    pub extern fn XOpenDisplay(
        display_name: [*:0]const u8, // [*:0]const c_char
    ) ?*Display;

    pub extern fn XQueryKeymap(
        display: ?*Display,
        keys_return: *[32]u8,
    ) i32; // c_int

    pub extern fn XQueryPointer(
        display: ?*Display,
        w: Window,
        root_return: *Window,
        child_return: *Window,
        root_x_return: *i32, // c_int
        root_y_return: *i32, // c_int
        win_x_return: *i32, // c_int
        win_y_return: *i32, // c_int
        mask_return: *u32, // c_uint
    ) i32; // bool

    pub extern fn XQueryTree(
        display: ?*Display,
        w: Window,
        root_return: *Window,
        parent_return: *Window,
        children_return: *?[*]Window,
        nchildren_return: *u32, // c_uint
    ) i32; // c_int

    pub extern fn XSetErrorHandler(
        handler: ?*const x11_types.XErrorHandler,
    ) ?*const x11_types.XErrorHandler;

    pub extern fn XStringToKeysym(
        string: [*:0]const u8, // [*:0]const c_char
    ) KeySym;
};

pub var XCloseDisplay: ?*const @TypeOf(X11.XCloseDisplay) = &X11.XCloseDisplay;
pub var XDefaultRootWindow: ?*const @TypeOf(X11.XDefaultRootWindow) = &X11.XDefaultRootWindow;
pub var XFree: ?*const @TypeOf(X11.XFree) = &X11.XFree;
pub var XGetInputFocus: ?*const @TypeOf(X11.XGetInputFocus) = &X11.XGetInputFocus;
pub var XGetWindowAttributes: ?*const @TypeOf(X11.XGetWindowAttributes) = &X11.XGetWindowAttributes;
pub var XGetWindowProperty: ?*const @TypeOf(X11.XGetWindowProperty) = &X11.XGetWindowProperty;
pub var XInternAtom: ?*const @TypeOf(X11.XInternAtom) = &X11.XInternAtom;
pub var XKeysymToKeycode: ?*const @TypeOf(X11.XKeysymToKeycode) = &X11.XKeysymToKeycode;
pub var XOpenDisplay: ?*const @TypeOf(X11.XOpenDisplay) = &X11.XOpenDisplay;
pub var XQueryKeymap: ?*const @TypeOf(X11.XQueryKeymap) = &X11.XQueryKeymap;
pub var XQueryPointer: ?*const @TypeOf(X11.XQueryPointer) = &X11.XQueryPointer;
pub var XQueryTree: ?*const @TypeOf(X11.XQueryTree) = &X11.XQueryTree;
pub var XSetErrorHandler: ?*const @TypeOf(X11.XSetErrorHandler) = &X11.XSetErrorHandler;
pub var XStringToKeysym: ?*const @TypeOf(X11.XStringToKeysym) = &X11.XStringToKeysym;

pub fn load_libx11_from_path(_: []const u8) !void {}
pub fn load_libx11() !void {}
pub fn unload_libx11() void {}
