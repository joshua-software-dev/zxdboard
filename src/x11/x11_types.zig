pub const Atom = enum(usize) {
    null_handle = 0,
    xa_primary = 1,
    xa_secondary = 2,
    xa_arc = 3,
    xa_atom = 4,
    xa_bitmap = 5,
    xa_cardinal = 6,
    _,
};
pub const Display = opaque {};
pub const KeyCode = u8;
pub const KeySym = usize;
pub const Window = enum(usize) { null_handle = 0, _ };

pub const XErrorHandler = fn (display: ?*Display, error_event: ?*anyopaque) callconv(.C) i32;
pub const XWindowAttributes = extern struct {
    x: i32,
    y: i32,
    width: i32,
    height: i32,
    border_width: i32,
    depth: i32,
    visual: ?*anyopaque,
    root: Window,
    class: i32,
    bit_gravity: i32,
    win_gravity: i32,
    backing_store: i32,
    backing_planes: usize, // c_ulong
    backing_pixel: usize, // c_ulong
    save_under: i32,
    colormap: usize,
    map_installed: i32,
    map_state: i32,
    all_events_masks: isize, // c_long
    your_events_mask: isize, // c_long
    do_not_propagate_mask: isize, // c_long
    override_redirect: i32,
    screen: ?*anyopaque,
};
