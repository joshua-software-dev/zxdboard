const builtin = @import("builtin");
const std = @import("std");

const x11_types = @import("x11_types.zig");
const x = @import("x11_wrapper.zig");

const Display = x11_types.Display;
const KeySym = x11_types.KeySym;

pub fn keybind_array_to_keysym_array(
    allocator: std.mem.Allocator,
    keybinds: []const []const u8,
) ![]const KeySym {
    if (comptime builtin.object_format != .elf) {
        @compileError("Non-Elf based Operating System unsupported");
    }

    var buf: [64]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buf);
    var out = std.ArrayList(KeySym).init(allocator);
    defer out.deinit();

    for (keybinds) |keybind| {
        fba.reset();
        const key_string: [:0]const u8 = try fba.allocator().dupeZ(u8, keybind);
        const keysym = x.string_to_keysym(key_string);
        if (keysym == 0) return error.FailedToParseKeybind;
        try out.append(keysym);
    }

    return out.toOwnedSlice();
}

const KeyCodeInfo = packed struct {
    // Once we know which byte we need from the keyboard_state, these three
    // bits serve as an index to which bit in that byte represents the pressed
    // state of the key this key_code represents.
    bit_index: u3,
    // These five bits of the key_code represent a number between 0 and 31.
    // This is used as an index into the keyboard_state to determine which byte
    // contains this key_code.
    keyboard_state_index: u5,
};

pub fn are_all_keysym_pressed_unmanaged(display: *Display, keys: []const KeySym) bool {
    if (comptime builtin.object_format != .elf) {
        @compileError("Non-Elf based Operating System unsupported");
    }
    if (keys.len == 0) return false;

    const keyboard_state = x.query_keymap(display);

    var pressed_count: usize = 0;
    for (keys) |key| {
        const key_code = x.keysym_to_keycode(display, key);
        const info: KeyCodeInfo = @bitCast(key_code);
        const states: std.bit_set.IntegerBitSet(8) = .{ .mask = keyboard_state[info.keyboard_state_index] };
        pressed_count += @intFromBool(states.isSet(info.bit_index));
    }

    return pressed_count == keys.len;
}
