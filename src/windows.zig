const std = @import("std");

const keystate = @import("windows/keystate.zig");
const win_types = @import("windows/win_types.zig");

pub const KeySym = win_types.KeySym;

pub const keybind_array_to_keysym_array = keystate.keybind_array_to_keysym_array;
pub const are_all_keysym_pressed = keystate.are_all_keysym_pressed;

/// This makes no allocations, and an allocator is merely required to match the
/// function signature of the posix implementation.
pub fn init(_: std.mem.Allocator) !void {}
pub fn deinit() void {}
