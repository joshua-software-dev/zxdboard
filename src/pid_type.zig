const builtin = @import("builtin");
const std = @import("std");

pub const pid_t = switch (builtin.os.tag) {
    .linux => std.os.linux.pid_t,
    .windows => @typeInfo(@TypeOf(std.os.windows.kernel32.GetCurrentProcessId)).Fn.return_type.?,
    else => i32,
};
