const builtin = @import("builtin");
const std = @import("std");

const zxdboard = @import("zxdboard");

const KEY_STRING = switch (builtin.os.tag) {
    .windows => "RSHIFT+F12",
    else => "Shift_R+F12",
};

pub fn main() !void {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    try zxdboard.init(allocator);
    defer zxdboard.deinit();

    switch (builtin.os.tag) {
        .linux => {
            const focused_window = zxdboard.get_focused_window();
            const pid = zxdboard.try_get_window_pid(focused_window);
            std.debug.print("pid: {d}\n", .{pid orelse 0});

            const window = zxdboard.search_all_windows_for_pid(std.os.linux.getpid());
            std.debug.print("window: {d}\n", .{window orelse .null_handle});
        },
        else => {},
    }

    const key_bindings = try zxdboard.keybind_array_to_keysym_array(
        allocator,
        switch (builtin.os.tag) {
            .windows => &.{ "RSHIFT", "F12" },
            else => &.{ "Shift_R", "F12" },
        },
    );
    defer allocator.free(key_bindings);

    while (true) {
        const is_pressed = zxdboard.are_all_keysym_pressed(key_bindings);
        if (is_pressed) {
            std.debug.print("KEYS ARE HELD: <{s}>\n", .{KEY_STRING});
        }
    }
}
