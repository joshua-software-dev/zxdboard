const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const libx11_path = if (b.option(
        []const u8,
        "libx11_path",
        "The full path to the libX11.so that will be loaded at runtime, default=null",
    )) |opt|
        opt
    else
        null;

    const use_system_x11 = if (b.option(
        bool,
        "use_system_x11",
        "Link and use system X11 library, rather than loading symbols dynamically, default=false",
    )) |opt|
        opt
    else
        false;

    const options = b.addOptions();
    options.addOption(bool, "use_system_x11", use_system_x11);
    options.addOption(?[]const u8, "libx11_path", libx11_path);

    const zxdboard = b.addModule(
        "zxdboard",
        switch (target.result.os.tag) {
            .windows => .{
                .root_source_file = b.path("src/windows.zig"),
                .target = target,
                .optimize = optimize,
            },
            else => .{
                .root_source_file = b.path("src/x11.zig"),
                .target = target,
                .optimize = optimize,
                .imports = &.{.{
                    .name = "zxdboard_options",
                    .module = options.createModule(),
                }},
            },
        },
    );

    if (use_system_x11) {
        switch (target.result.ptrBitWidth()) {
            32 => zxdboard.addLibraryPath(.{ .cwd_relative = "/usr/lib32/" }),
            else => zxdboard.addLibraryPath(.{ .cwd_relative = "/usr/lib/" }),
        }
        zxdboard.linkSystemLibrary("X11", .{ .needed = true });
    }

    if (target.result.os.tag != .windows) {
        zxdboard.link_libc = true;
    }

    const exe = b.addExecutable(.{
        .name = "zxdboard_test",
        .root_source_file = b.path("src/exe.zig"),
        .target = target,
        .optimize = optimize,
    });
    exe.root_module.addImport("zxdboard", zxdboard);

    b.installArtifact(exe);

    // This *creates* a Run step in the build graph, to be executed when another
    // step is evaluated that depends on it. The next line below will establish
    // such a dependency.
    const run_cmd = b.addRunArtifact(exe);

    // By making the run step depend on the install step, it will be run from the
    // installation directory rather than directly from within the cache directory.
    // This is not necessary, however, if the application depends on other installed
    // files, this ensures they will be present and in the expected location.
    run_cmd.step.dependOn(b.getInstallStep());

    // This allows the user to pass arguments to the application in the build
    // command itself, like this: `zig build run -- arg1 arg2 etc`
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    // This creates a build step. It will be visible in the `zig build --help` menu,
    // and can be selected like this: `zig build run`
    // This will evaluate the `run` step rather than the default, which is "install".
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
