# zxdboard

A dead simple library for checking the currently pressed keys in X11 contexts or on Windows. Can optionally (enabled by default) `dlopen` X11 to avoid the large dynamic dependency tree this would otherwise incur if linked normally.

# LICENSE

Significant portions of this codebase are heavily inspired / ported from Mangohud. As such, its LICENSE is included in this repo for compliance. The many LICENSEs in XOrg's COPYING are also included to comply with their terms.

# Including in your project:

In build.zig:
```zig
const zxdboard_dep = b.dependency("zxdboard", .{
    .target = target,
    .optimize = optimize,
});

const exe = b.addExecutable(.{
    .name = "example",
    .root_source_file = .{ .path = "src/main.zig" },
    .target = target,
    .optimize = optimize,
});

if (!target.isWindows())
{
    // for dlopen to search your system library paths, you must link libc
    exe.linkLibC();
}
exe.addModule("zxdboard", zxdboard_dep.module("zxdboard"))
```

alternatively, if you want to dynamically link X11 rather than `dlopen` it:
```zig
const zxdboard_dep = b.dependency("zxdboard", .{
    .target = target,
    .optimize = optimize,
    .use_system_x11 = true,
});

const exe = b.addExecutable(.{
    .name = "example",
    .root_source_file = .{ .path = "src/main.zig" },
    .target = target,
    .optimize = optimize,
});

if (!target.isWindows())
{
    exe.linkSystemLibrary("X11");
}
exe.addModule("zxdboard", zxdboard_dep.module("zxdboard"))
```

and if you're on a non-linux platform that still uses elf files and libX11, are fine with `dlopen`, but don't want to link libc:
```zig
// This doesn't work on glibc linux, sadly.
// It might work on a dynamic musl distro?
// https://github.com/ziglang/zig/issues/5360
const zxdboard_dep = b.dependency("zxdboard", .{
    .target = target,
    .optimize = optimize,
    .libx11_path = "/path/to/libX11.so",
});

const exe = b.addExecutable(.{
    .name = "example",
    .root_source_file = .{ .path = "src/main.zig" },
    .target = target,
    .optimize = optimize,
});
exe.addModule("zxdboard", zxdboard_dep.module("zxdboard"))
```

check `src/exe.zig` for a basic usage example.
